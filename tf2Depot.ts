import SteamUser from 'steam-user';

let manifestInterval: NodeJS.Timer | null
let user: SteamUser | undefined
let connected = false
let lastError: string | undefined
const depots: Record<string, any> = {}
let lastCheck = 0

function go() {
  user = new SteamUser()

  user.logOn()
  setTimeout(() => {
    if (!connected) {
      console.log('Failed to connect after 1 minute')
      process.exit(1)
    }
  }, 60000)
  user.on('loggedOn', () => {
    connected = true
    get440DepotInfo()
    manifestInterval = setInterval(get440DepotInfo, 300000)
  })
  user.on('disconnected', () => {
    connected = false
    clearInterval(manifestInterval!)
    manifestInterval = null
    setTimeout(() => process.exit(1), 5000)
  })
}

async function get440DepotInfo() {
  try {
    const result = await user!.getProductInfo([440, 228980], [228980])
    lastCheck = Date.now()
    Object.keys(result.apps[440].appinfo.depots).forEach(key => {
      const depot = result.apps[440].appinfo.depots[key]
      if (depot.manifests) {
        depots[key] = depot.manifests.public
      } else if (key === '228990') {
        depots['228990'] = result.apps[228980].appinfo.depots[228990].manifests.public
      }
    })
    lastError = undefined
  } catch (e: any) {
    lastError = `Failed updating appOwnership because ${e} at ${e.stack}`; console.log(lastError)
  }
}

export function makeAppmanifest440() {
  if (!depots['440']) {
    return undefined
  }

  return `"AppState"
  {
    "appid"		"440"
    "Universe"		"1"
    "name"		"Team Fortress 2"
    "StateFlags"		"4"
    "installdir"		"Team Fortress 2"
    "LastUpdated"		"0"
    "UpdateResult"		"0"
    "SizeOnDisk"		"0"
    "buildid"		"0"
    "LastOwner"		"0"
    "BytesToDownload"		"0"
    "BytesDownloaded"		"0"
    "AutoUpdateBehavior"		"0"
    "AllowOtherDownloadsWhileRunning"		"0"
    "ScheduledAutoUpdate"		"0"
    "MountedDepots"
    {
      "441"		"${depots['441']}"
      "440"		"${depots['440']}"
      "232251"		"${depots['232251']}"
    }
    "SharedDepots"
    {
      "228990"		"228980"
    }
    "UserConfig"
    {
      "language"		"english"
    }
  }`
}

export function makeAppmanifest228980() {
  if (!depots['440']) {
    return undefined
  }

  return `"AppState"
  {
    "appid"		"228980"
    "Universe"		"1"
    "name"		"Steamworks Common Redistributables"
    "StateFlags"		"70"
    "installdir"		"Steamworks Shared"
    "LastUpdated"		"1559633450"
    "UpdateResult"		"0"
    "SizeOnDisk"		"102931551"
    "buildid"		"3884457"
    "LastOwner"		"0"
    "BytesToDownload"		"0"
    "BytesDownloaded"		"0"
    "AutoUpdateBehavior"		"0"
    "AllowOtherDownloadsWhileRunning"		"0"
    "UserConfig"
    {
    }
    "MountedDepots"
    {
      "228990"		"${depots['228990']}"
    }
    "InstallScripts"
    {
      "0"		"_CommonRedist\\DirectX\\Jun2010\\installscript.vdf"
    }
  }`
}

export function checkHealth() {
  if (Date.now() - lastCheck > (1000 * 60 * 30)) {
    return { status: 'CRITICAL', message: 'Last check was over 30 minutes ago' }
  }
  const depotNums = ['441', '440', '232251', '228990']
  for (let i = 0; i < depotNums.length; i++) {
    const depot = depotNums[i]
    if (!depots[depot as unknown as number]) {
      return { status: 'CRITICAL', message: 'One of the required depots is missing a manifest id' }
    }
  }
  return { status: 'OK!' }
}

export function getDepots() {
  return depots
}

go()