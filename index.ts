import 'source-map-support/register'

import express from 'express'
import bodyParser from 'body-parser'
import { get, post } from '@spacepumpkin/minimal-request'
import cors from 'cors'
import { dongers, getRandom } from '@spacepumpkin/dongers'
import { getZalgoString } from './zalgo'
import { quotes } from './quotes'
import { genUsername } from './username'
import { checkHealth, getDepots, makeAppmanifest228980, makeAppmanifest440 } from './tf2Depot'
import { getImdbInfo } from '@spacepumpkin/imdb-api'
import { tv, movie } from "@spacepumpkin/scene-parser"
import colours from "@spacepumpkin/home-leds-colour"
import specificId from "@spacepumpkin/tf-specific-id"
import itemSchema from "@spacepumpkin/tf-itemschema"
import ytdl from "@spacepumpkin/ytdl-core";

const app = express()
const startTime = Date.now()
app.use(bodyParser.json({ limit: '10mb' }))
app.use(cors())

itemSchema.setSteamApiKey('F08663AFE6C313DCEBBB5EEEF498C072')
itemSchema.fetchItemSchema()

app.get('/', function (req, res) {
  setStatusCodeAndBody(res, 200, {
    uptime: convertToTimestamp(Date.now() - startTime),
    endpoints: {
      "Colour": {
        GET: {
          '/colours/kelvin/:kelvin': 'Get RGB values for a given kelvin temperature (ie, /colours/kelvin/2000)',
          '/colours/hex/:hex': 'Convert a hex string to RGB values (ie, /colours/hex/#ffffff)',
          '/colours/rgb': 'Convert RGB values to a hex string (ie, colours/rgb?r=255&g=255&b=255)',
          '/colours/scaleRgb': 'Ensure an RGB colour is equal or dimmer than a max brightness (0-1) (ie, colours/scaleRgb?r=255&g=255&b=255&maxBrightness=0.5)'
        },
        POST: {
          '/colours/rgb': {
            description: 'Convert RGB values to a hex string (ie, colours/rgb?r=255&g=255&b=255)',
            example: {
              url: '/colours/rgb',
              body: {
                r: 255,
                g: 255,
                b: 255
              },
              returns: {
                input: {
                  r: 255,
                  g: 255,
                  b: 255
                },
                hex: '#ffffff'
              }
            }
          },
          '/colours/scaleRgb': {
            description: 'Ensure an RGB colour is equal or dimmer than a max brightness (0-1)',
            example: {
              url: '/colours/scaleRgb',
              body: {
                r: 255,
                g: 255,
                b: 255,
                maxBrightness: 0.5
              },
              returns: {
                input: {
                  r: 255,
                  g: 255,
                  b: 255
                },
                output: {
                  r: 127,
                  g: 127,
                  b: 127
                }
              }
            }
          },
        }
      },
      "Donger": {
        GET: {
          '/donger': 'Get help for the donger endpoint',
          '/donger/:type': 'Get a random donger to represent the type/emotion, or use "all" to get all dongers'
        }
      },
      "IMDb": {
        GET: {
          '/imdb/:imdbId': 'Get data for IMDb id'
        }
      },
      "Quote": {
        GET: {
          '/donger': 'Get help for the quote endpoint',
          '/donger/:type': 'Get a random quote from a person, or use "all" to get all qutes from everyone supported'
        }
      },
      "Scheme Parser": {
        GET: {
          '/scene/tv?name=NAME': 'Interpret a scene TV release name into a standard JSON object (ie, /scene/tv?name=Fatal.Fiance.2021.HDTV.x264-CRiMSON)',
          '/scene/movie?name=NAME': 'Interpret a scene movie release name into a standard JSON object (ie, /scene/movie?name=Tom.and.Jerry.2021.720p.BluRay.x264.DTS-FGT)'
        }
      },
      "SpecificId": {
        GET: {
          '/specificId/fromMarketHashName/:name': 'Get a specificId from a Market Hash Name (ie, /specificId/fromMarketHashName/Mann Co. Supply Crate Key?tradable=true&craftable=true)',
          '/specificId/:id': 'Get details about a specificId (ie, /specificId/5021;6)'
        }
      },
      "TF2 Depot Checker": {
        GET: {
          '/depots/appmanifest_440.acf': 'The constructed appmanifest_440.acf (TF2) that goes in SteamApps',
          '/depots/appmanifest_228980.acf': 'The constructed appmanifest_228980.acf (DirectX Redist) that goes in SteamApps',
          '/depots': 'Get health status of the depot checker bot'
        }
      },
      "Username": {
        GET: {
          '/username?length=X&leetChance=Y': 'Generate a random username of length X with a chance to replace a letter with its leet form number (0-100) (ie, /username?length=25&leetChance=50 to generate a username of 25 characters long with a 50/50 chance to replace letters with numbers)',
        }
      },
      "ytdl": {
        GET: {
          '/ytdl?id=X': 'Call ytdl for video info - provide an x-cookie header to use a custom cookie in the request',
        }
      },
      "Zalgo": {
        GET: {
          '/zalgo': 'Get help for the zalgo endpoint',
          '/zalgo?text': 'Corrupt some input text (ie, /zalgo?text=text to corrupt)',
          '/zalgo/:text': 'Corrupt some input text (ie, /zalgo/text to corrupt)'
        }
      }
    }
  })
})

app.get('/donger', (req, res) => {
  setStatusCodeAndBody(res, 200, `GET /donger/:type where type can be any one of ${Object.keys(dongers)}`)
})

app.get('/donger/:type', (req, res) => {
  if (req.params.type === 'all') {
    return setStatusCodeAndBody(res, 200, dongers)
  }
  //@ts-ignore  
  const arr = dongers[req.params.type]
  if (arr) {
    return setStatusCodeAndBody(res, 200, getRandom(arr))
  }
  return setStatusCodeAndBody(res, 400, `Donger emotion not found. Valid emotions: ${Object.keys(dongers)}`)
})

app.get('/zalgo', (req, res) => {
  const text = req.query.text || req.query.str || req.query.string || req.query.in || req.query.input
  if (!text) {
    return setStatusCodeAndBody(res, 400, 'GET /zalgo/:text or /zalgo?text=TEXT to corrupt text')
  }
  return setStatusCodeAndBody(res, 200, getZalgoString(text.toString()))
})

app.get('/zalgo/:text', (req, res) => {
  setStatusCodeAndBody(res, 200, getZalgoString(req.params.text))
})

app.get('/quote', (req, res) => {
  setStatusCodeAndBody(res, 200, `GET /quote/:type where type can be any one of ${Object.keys(quotes)}`)
})

app.get('/quote/:type', (req, res) => {
  if (req.params.type === 'all') {
    return setStatusCodeAndBody(res, 200, quotes)
  }
  //@ts-ignore  
  const arr = quotes[req.params.type]
  if (arr) {
    return setStatusCodeAndBody(res, 200, getRandom(arr))
  }
  return setStatusCodeAndBody(res, 400, `Available types: ${Object.keys(quotes)}`)
})

app.get('/username', (req, res) => {
  const length = Number(req.query.length) || 20
  const leetChance = Number(req.query.leetChance) || 25
  setStatusCodeAndBody(res, 200, genUsername(length, leetChance))
})

app.get('/specificId/fromMarketHashName/:name', (req, res) => {
  if (!itemSchema.isSchemaReady()) {
    return setStatusCodeAndBody(res, 500, 'Item Schema is not ready yet')
  }
  const name = req.params.name
  if (!name) {
    return setStatusCodeAndBody(res, 400, 'Provide a market hash name (ie, /specificId/fromMarketHashName/Mann Co. Supply Crate Key?tradable=true&craftable=true)')
  }
  const tradable = req.query.tradable ? req.query.tradable.toString().toLowerCase() === "true" : true
  const craftable = req.query.craftable ? req.query.craftable.toString().toLowerCase() === "true" : true
  const raw: any = specificId.getFromMarketHashName(name, craftable, req.query.unusualId, tradable)
  return setStatusCodeAndBody(res, 200, raw.getObject())
})

app.get('/specificId/:id', (req, res) => {
  if (!itemSchema.isSchemaReady()) {
    return setStatusCodeAndBody(res, 500, 'Item Schema is not ready yet')
  }
  const id = req.params.id
  if (!id) {
    return setStatusCodeAndBody(res, 400, 'Provide a specificId (ie, /specificId/5021;6)')
  }
  const obj = specificId.fromOwnId(id)
  const name = specificId.getNameFromSpecificId(obj)
  return setStatusCodeAndBody(res, 200, { ...obj.getObject(), name })
})

let address: string | null = null
getAddress()
setInterval(getAddress, 60000)
app.post('/request', async (req, res) => {
  const { url, method, requestOptions, responseOptions } = req.body
  if (!url || !method) {
    return setStatusCodeAndBody(res, 400, 'Missing url or method')
  }
  let response = null
  try {
    if (method.toLowerCase() === 'get') {
      response = await get(url, requestOptions, responseOptions)
    } else {
      response = await post(url, requestOptions, responseOptions)
    }
    setStatusCodeAndBody(res, 200, rebuildResponse(response))
  } catch (e) {
    setStatusCodeAndBody(res, 500, {})
  }
})

app.get('/depots/appmanifest_440.acf', (req, res) => {
  const data = makeAppmanifest440()
  if (!data) {
    return setStatusCodeAndBody(res, 500, 'Depot Checker is not working at the moment')
  }
  setStatusCodeAndBody(res, 200, data)
})

app.get('/depots/appmanifest_228980.acf', (req, res) => {
  const data = makeAppmanifest228980()
  if (!data) {
    return setStatusCodeAndBody(res, 500, 'Depot Checker is not working at the moment')
  }
  setStatusCodeAndBody(res, 200, data)
})

app.get('/depots', (req, res) => {
  setStatusCodeAndBody(res, 200, { ...checkHealth(), depots: getDepots() })
})

app.get('/imdb/:id', async (req, res) => {
  if (!req.params.id) {
    return setStatusCodeAndBody(res, 400, 'Provide an IMDb Id in path')
  }

  try {
    const data = await getImdbInfo(req.params.id)
    return setStatusCodeAndBody(res, 200, data)
  } catch (e: any) {
    return setStatusCodeAndBody(res, 500, e.message)
  }
})

app.get('/scene/tv', (req, res) => {
  const filename = req.query.filename || req.query.name
  if (!filename) {
    return setStatusCodeAndBody(res, 400, 'Expected a name parameter (ie, /scene/tv?name=Fatal.Fiance.2021.HDTV.x264-CRiMSON')
  }

  try {
    return setStatusCodeAndBody(res, 200, tv(filename as string))
  } catch (e: any) {
    return setStatusCodeAndBody(res, 500, { error: e.message })
  }
})

app.get('/scene/movie', (req, res) => {
  const filename = req.query.filename || req.query.name
  if (!filename) {
    return setStatusCodeAndBody(res, 400, 'Expected a name parameter (ie, /scene/movie?name=Tom.and.Jerry.2021.720p.BluRay.x264.DTS-FGT')
  }

  try {
    return setStatusCodeAndBody(res, 200, movie(filename as string))
  } catch (e: any) {
    return setStatusCodeAndBody(res, 500, { error: e.message })
  }
})

app.get('/colours/kelvin/:kelvin', (req, res) => {
  setStatusCodeAndBody(res, 200, colours.kelvinToRgb(req.params.kelvin))
})

app.get('/colours/hex/:hex', (req, res) => {
  setStatusCodeAndBody(res, 200, colours.hexToRgb(req.params.hex))
})

app.get('/colours/rgb', (req, res) => {
  const { r, g, b } = req.query
  const input = { r: Number(r) || 0, g: Number(g) || 0, b: Number(b) || 0 }
  const hex = colours.rgbToHex(input)
  setStatusCodeAndBody(res, 200, { input, hex })
})

app.post('/colours/rgb', (req, res) => {
  const { r, g, b } = req.body
  const input = { r: r || 0, g: g || 0, b: b || 0 }
  const hex = colours.rgbToHex(input)
  setStatusCodeAndBody(res, 200, { input, hex })
})

app.get('/colours/scaleRgb', (req, res) => {
  const { r, g, b, maxBrightness } = req.query
  const input = { r: Number(r) || 0, g: Number(g) || 0, b: Number(b) || 0 }
  const output = colours.scaleRgb(input, maxBrightness)
  setStatusCodeAndBody(res, 200, { input, output })
})

app.post('/colours/scaleRgb', (req, res) => {
  const { r, g, b, maxBrightness } = req.body
  const input = { r: r || 0, g: g || 0, b: b || 0 }
  const output = colours.scaleRgb(input, maxBrightness)
  setStatusCodeAndBody(res, 200, { input, output })
})

app.get('/ytdl', async (req, res) => {
  const id = req.query.id

  if (typeof (id) !== 'string' || !id) {
    return setStatusCodeAndBody(res, 400, { error: 'id was malformed' })
  }

  const cookie = req.body.cookie || req.headers['x-cookie'] || ''

  try {
    const data = await retryWebRequest(`getInfo ${req.query.id}`, () => ytdl.getInfo(id, { requestOptions: { headers: { cookie } } }))
    setStatusCodeAndBody(res, 200, { request: { id, cookie }, response: data })
  } catch (e: any) {
    setStatusCodeAndBody(res, 500, { request: { id, cookie }, response: { error: e.stack, message: e.message } })
  }
})

app.post('/ytdl', async (req, res) => {
  const id = req.query.id

  if (typeof (id) !== 'string' || !id) {
    return setStatusCodeAndBody(res, 400, { error: 'id was malformed' })
  }

  const cookie = req.body.cookie || req.headers['x-cookie'] || ''

  try {
    const data = await retryWebRequest(`getInfo ${req.query.id}`, () => ytdl.getInfo(id, { requestOptions: { headers: { cookie } } }))
    setStatusCodeAndBody(res, 200, { request: { id, cookie }, response: data })
  } catch (e: any) {
    setStatusCodeAndBody(res, 500, { request: { id, cookie }, response: { error: e.stack, message: e.message } })
  }
})

function rebuildResponse(response: any) {
  return {
    body: response.body,
    client: {
      localAddress: address,
      remoteAddress: response.client.remoteAddress,
      remotePort: response.client.remotePort
    },
    cookies: response.cookies,
    headers: response.headers,
    rawHeaders: response.rawHeaders,
    statusCode: response.statusCode,
    statusMessage: response.statusMessage
  }
}

async function getAddress() {
  const response = await get('https://api.ipify.org')
  address = response.body
}

function setStatusCodeAndBody(res: any, code: number, body: string | object) {
  res.statusCode = code
  if (typeof (body) === 'object') {
    res.setHeader('content-type', 'application/json')
    body = JSON.stringify(body)
  }
  res.send(body)
}

// Format the up time in a human readable way
function convertToTimestamp(time: number) {
  let upTime = time
  if (time < 0) return `${time} ms`
  const days = Math.floor(upTime / (1000 * 60 * 60 * 24))
  upTime -= days * (1000 * 60 * 60 * 24)

  const hours = Math.floor(upTime / (1000 * 60 * 60))
  upTime -= hours * (1000 * 60 * 60)

  const mins = Math.floor(upTime / (1000 * 60))
  upTime -= mins * (1000 * 60)

  if (days === 0) {
    const seconds = Math.floor(upTime / (1000))
    upTime -= seconds * (1000)
    return `${hours} hours, ${mins} mins, ${seconds} seconds`
  } else {
    return `${days} days, ${hours} hours, ${mins} minutes`
  }
}

function wait(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms))
}

const RETRYABLE_NETWORK_ERRORS = ['ECONNRESET', 'ETIMEDOUT', 'EAI_AGAIN', 'secure TLS', 'socket hang up']
async function retryWebRequest(alias: string, fn: () => Promise<any>) {
  while (true) {
    try {
      return await fn()
    } catch (e: any) {
      if (!RETRYABLE_NETWORK_ERRORS.some(o => e.stack.includes(o))) {
        throw e
      }
      console.error(`Attempting to retry a failure for ${alias}`)
      await wait(1000)
    }
  }
}

app.listen(process.env.PORT || 80)