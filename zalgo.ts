import zalgo from "zalgo-js"

export function getZalgoString(input: string) {
  return zalgo(input)
}