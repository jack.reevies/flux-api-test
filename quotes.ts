export const quotes = {
  scooter: [
    'Coffee isnt my cup of tea',
    'Diamonds are made under pressure',
    'Its nice to be important, but its more important to be nice',
    'The cherries are not important',
    'Lets blow a hole in the bowl',
    'Shouting is for the family',
    'Respect to the man in the ice cream van',
    'How do I get off the bus?',
    'The only way to have a friend is to be a friend',
    'Take care to get what you like, or you will be forced to like what you get',
    'Gothic doesnt exist',
    'Knowledge speaks but wisdom listens',
    'Having fun is my religion',
    'Don\'t take life too seriously, nobody gets out alive anyway!',
    'Life without knowledge is death in disguise'
  ],
  danganronpa: [
    'It is not the strong or the smart that survive, but the ones who can bring about change',
    'Living in despair really isn\'t living at all',
    'If the world outside is void of hope, and if despair is contagious, then I\'ll just have to infect everyone with my hope',
    'Real strength only reveals itself when one is faced with great turmoil! Therefore, I shall always tread a thorny path',
    'A hint is one thing, but simply sharing the answer it leads to is dangerous',
    'Wherever there is hope, there is most definitely despair',
    'It\'s okay to feel depressed. It takes time to overcome things. And then, by taking that time, you just start moving forward again',
    'If you make light of other people\'s emotions, it will surely come back to bite you',
    'There is no greater accomplishment in life than pushing yourself to the limit',
    'Without imagination, you can never deduce which action to take next',
    'Fear is proof that your imagination is functioning',
    'Everyone just pretends to understand... and to be understood',
    'No matter who the enemy may be, those who can\'t adapt will be the first to die',
    'The fear of invisible treachery becomes the greatest enemy of stability',
    'It\'s not geniuses that change the world. It\'s ordinary people who make every effort they can',
    'It doesn\'t matter how negative something is. Just add \'lol\' and it\'ll automatically become positive'
  ],
  misc: [
    'Knowing you\'re different is only the beginning. If you accept these differences you\'ll be able to get past them and grow even closer',
    'You can die anytime, but living takes true courage',
    'If nobody cares to accept you and wants you in this world, accept yourself and you will see that you don\'t need them and their selfish ideas',
    'It\'s more important to master the cards you’re holding than to complain about the ones your opponent was dealt',
    'You can\'t win a game by doing nothing. And if someone else wins it for you then you haven\'t accomplished anything. Life is the same way',
    'Some people live more in 20 years than others do in 80. It\'s not the time that matters, it\'s the person',
    'There\'s no point in being grown up if you can\'t be childish sometimes',
    'A straight line may be the shortest distance between two points, but it is by no means the most interesting',
    'One may tolerate a world of demons for the sake of an angel',
    'Courage isn\'t just a matter of not being frightened. It\'s being afraid and doing what you have to do anyway',
    'The very powerful and the very stupid have one thing in common. They don\'t alter their views to fit the facts. They alter the facts to fit the views',
    'The least important things sometimes lead to the greatest discoveries'
  ],
  drumsy: [
    'If she touches your bellybutton and squeezes your left testicle at the same time, you\'ll take a screenshot',
    'If the early bird gets the worm, the singing bird gets your sperm',
    'Don\'t pee in the water, unless its for your daughter'
  ]
}