declare module '@spacepumpkin/minimal-request'

interface RequestOptions {
  headers?: any,
  cookies?: any,
  body?: any | String,
  contentType?: String,
  method?: 'POST' | 'GET' | 'DELETE' | 'PUT'
}

interface ResponseOptions {
  encoding?: String | 'utf8'
}

interface WebResponse extends IncomingMessage {
  body?: any | String,
  cookies?: any
}
